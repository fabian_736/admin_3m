@extends('layouts.admin.app')

@section('content')




    <div class="row">
        <div class="col-md-12">

            <div class="card-header">
                <label for="">Listado de clientes</label>
            </div>
            <div class="card-body">

                <table class="table table-bordered" id="table1">
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Ciudad</th>
                            <th>Tipo de odontologo</th>
                            <th>Ciudad</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-light">
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->USER }}</td>
                                <td>{{ $user->NOMBRES }}</td>
                                <td>{{ $user->APELLIDOS }}</td>
                                <td>{{ $user->CIUDAD }}</td>
                                <td>{{ $user->TIPO_ODONTOLOGO_USER }}</td>
                                <td>{{ $user->CIUDAD }}</td>
                                <td>
                                    <form action="" method="post">
                                        <div class="row">
                                            <a href="{{ route('user.edit', $user->ID_USUARIO) }}"
                                                class="btn btn-sm btn-warning mx-auto"><i class="far fa-edit"></i></a>
                                            <a href="{{ route('user.confirm', $user->ID_USUARIO) }}"
                                                class="btn btn-sm btn-danger mx-auto"><i class="far fa-trash-alt"></i></a>
                                        </div>

                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>



        </div>
    </div>



@endsection
