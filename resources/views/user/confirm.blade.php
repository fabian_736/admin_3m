@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        <h1 class="mb-5">¿Deseas eliminar el siguiente usuario? </h1>
        <h4>Nombre de usuario: </h4>
        <p>{{ $user->USER }}</p>
        <h4>Tipo de odontologo: </h4>
        <p>{{ $user->TIPO_ODONTOLOGO_USER }}</p>
        <h4>Nombres: </h4>
        <p>{{ $user->NOMBRES }}</p>
        <h4>Apellidos: </h4>
        <p>{{ $user->APELLIDOS }}</p>
        <form method="post" enctype="multipart/form-data" action="{{ route('user.destroy', $user->ID_USUARIO) }}">
            @method('DELETE')
            @csrf
            <div class="row col-md-12 mt-5 mx-auto">
                <button type="submit" class="redondo btn btn-danger col-md-4 mx-auto">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
                <a href="{{ route('user.list') }}" class="btn btn-primary col-md-4 mx-auto">Volver</a>
            </div>
        </form>
    </div>
@endsection
