@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        <div class="row mb-5 mx-auto">
            <div class="col-md-10 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">
                <div class="row col-md-12 mt-3 mb-5">
                    <div class="row col-md-12 mx-auto"><label for="" class="lead">Escoge el tipo de usuario que quieres crear:</label></div>
                    <a href="javascript:;" class="col-md-4 btn btn-warning mx-auto mt-2"  onclick="myFunctionB1()">Asesor</a>
                    <a href="javascript:;" class="col-md-4 btn btn-primary mx-auto mt-2"  onclick="myFunctionB2()">Cliente</a>
                </div>

                <form action="{{ route('user.store') }}" method="post" id='segundo' style="display: none;">
                    @csrf
                    <div class="text-center">
                        <label for="" class="lead">Registrar Cliente</label>
                    </div>
                    <hr>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Nombre de usuario:</label>
                            <input name="USER" id="categoria" type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Contraseña:</label>
                            <input name="CONTRASEÑA" id="categoria" type="password" class="form-control">
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-6">
                            <label for="name_user">Nombres:</label>
                            <input name="NOMBRES" id="categoria" type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Apellidos:</label>
                            <input name="APELLIDOS" id="SUBCATEGORIA" type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Celular:</label>
                            <input name="CELULAR" id="categoria" type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Privilegios:</label>
                            <input name="PRIVILEGIOS" id="categoria" type="text" class="form-control" value="2" readonly>
                        </div>

                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Estado:</label>
                            <input name="ESTADO" id="categoria" type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Ciudad:</label>
                            <input name="CIUDAD" id="categoria" type="text" class="form-control">
                        </div>


                    </div>



                    <hr>
                    <div class="row col-md-12 my-3">
                        <button type="submit" class="btn btn-primary form-control">Crear usuario</button>
                    </div>
                </form>

                <form action="{{ route('user.store') }}" method="post" id='primero' style="display: none;">
                    @csrf
                    <div class="text-center">
                        <label for="" class="lead">Registrar Asesor</label>
                    </div>
                    <hr>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Nombre de usuario:</label>
                            <input name="USER" id="categoria" type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Contraseña:</label>
                            <input name="CONTRASEÑA" id="categoria" type="password" class="form-control">
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-6">
                            <label for="name_user">Nombres:</label>
                            <input name="NOMBRES" id="categoria" type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Apellidos:</label>
                            <input name="APELLIDOS" id="SUBCATEGORIA" type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Celular:</label>
                            <input name="CELULAR" id="categoria" type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Privilegios:</label>
                            <input name="PRIVILEGIOS" id="categoria" type="text" class="form-control" value="3" placeholder="3" readonly>
                        </div>

                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Tipo de odontologo:</label>
                            <select name="TIPO_ODONTOLOGO_USER" id="" class="form-control">
                                <option value="SIN INFORMACION" selected>Seleccione...</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Estado:</label>
                            <select name="ESTADO" id="" class="form-control">
                                <option value="SIN INFORMACION" selected disabled>Seleccione...</option>
                                <option value="Activo">Activo</option>
                                <option value="Inactivo">Inactivo</option>
                            </select>
                        </div>

                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-12">
                            <label for="name_user">Ciudad:</label>
                            <input name="CIUDAD" id="categoria" type="text" class="form-control">
                        </div>

                    </div>



                    <hr>
                    <div class="row col-md-12 my-3">
                        <button type="submit" class="btn btn-primary form-control">Crear usuario</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <script>
        function init(){
            var x = document.getElementById("primero");
            var y = document.getElementById("segundo");
            var z = document.getElementById("tercero");
            x.style.display = "block";
            y.style.display = "none";
            z.style.display = "none";

        }

        function myFunctionB1() {
            var x = document.getElementById("primero");
            var y = document.getElementById("segundo");
            var z = document.getElementById("tercero");
            if (x.style.display === "none") {
                x.style.display = "block";
                y.style.display = "none";
                z.style.display = "none";
            } else {
                x.style.display = "none";
                y.style.display = "none";
                z.style.display = "none";
            }

        }

        function myFunctionB2() {
            var x = document.getElementById("primero");
            var y = document.getElementById("segundo");
            var z = document.getElementById("tercero");
            if (y.style.display === "none") {
                y.style.display = "block";
                x.style.display = "none";
                z.style.display = "none";
            } else {
                x.style.display = "none";
                y.style.display = "none";
                z.style.display = "none";
            }
        }
    </script>


@endsection
