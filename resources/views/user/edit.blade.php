@extends('layouts.admin.app')

@section('content')

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-10 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">
                <form action="{{ route('user.update', $user->ID_USUARIO) }}" method="post">

                    @csrf
                    {{ method_field('PATCH') }}

                    <h3 class="mb-5">Editar usuarios</h3>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Nombre de usuario:</label>
                            <input name="USER" id="categoria" type="text" class="form-control" value="{{ $user->USER }}">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Contraseña:</label>
                            <input name="CONTRASEÑA" id="categoria" type="text" class="form-control"
                                value="{{ $user->CONTRASEÑA }}">
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-6">
                            <label for="name_user">Nombres:</label>
                            <input name="NOMBRES" id="categoria" type="text" class="form-control"
                                value="{{ $user->NOMBRES }}">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Apellidos:</label>
                            <input name="APELLIDOS" id="SUBCATEGORIA" type="text" class="form-control"
                                value="{{ $user->APELLIDOS }}">
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Celular:</label>
                            <input name="CELULAR" id="categoria" type="text" class="form-control"
                                value="{{ $user->CELULAR }}">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Estado:</label>
                            <input name="ESTADO" id="categoria" type="text" class="form-control"
                                value="{{ $user->ESTADO }}">
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Tipo de odontologo:</label>
                            <input name="TIPO_ODONTOLOGO_USER" id="categoria" type="text" class="form-control"
                                value="{{ $user->TIPO_ODONTOLOGO_USER }}">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Ciudad:</label>
                            <input name="CIUDAD" id="categoria" type="text" class="form-control"
                                value="{{ $user->CIUDAD }}">
                        </div>

                    </div>

                    <div class="row col-md-12">
                        <input class="btn btn-success my-3 form-control col-md-8 mx-auto" type="submit" value="Actualizar Usuario">
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection
