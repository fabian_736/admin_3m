<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; People Marketing S.A.S 2021</span>
        </div>
    </div>
</footer>
