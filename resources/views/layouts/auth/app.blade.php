<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="https://www.3m.com.co/favicon.ico">


    <title>3M | Oral Care</title>

    <script src="https://kit.fontawesome.com/eff9f4dcae.js" crossorigin="anonymous"></script>

    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

        <link rel="stylesheet" href="{{url('css/app.css')}}">
        
       

</head>

<body id="page-top">

    @yield('content')

<script src="{{url('js/app.js')}}"></script>
</body>

</html>