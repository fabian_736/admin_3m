@extends('layouts.admin.app')

@section('content')




        <div class="row">
            <div class="col-md-12">

<div class="card-header">
    <label for="">Listado de productos</label>
</div>
                <div class="card-body">

                    <table class="table table-bordered" id="table1">
                        <thead>
                        <tr>
                            <th>Categoria</th>
                            <th>Subcategoria</th>
                            <th>Stock</th>
                            <th>Descripcion</th>
                            <th>Estado</th>
                            <th>Precio</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody class="table-light">
                            @foreach($products as $product)
                                <tr>
                                    <td >{{ $product->CATEGORIA }}</td>
                                    <td >{{ $product->SUBCATEGORIA }}</td>
                                    <td >{{ $product->STOCK }}</td>
                                    <td >{{ $product->DESCRIPCION }}</td>
                                    <td >{{ $product->ESTADO }}</td>
                                    <td >{{ $product->PRECIO }}</td>
                                    <td>
                                        <form action="" method="post">
                                       <div class="row">
                                            <a href="{{ route('product.edit',$product->ID_CATEGORIA) }}" class="btn btn-sm btn-warning mx-auto"><i class="far fa-edit"></i></a>
                                            <a href="{{ route('product.confirm',$product->ID_CATEGORIA) }}" class="btn btn-sm btn-danger mx-auto"><i class="far fa-trash-alt"></i></a>
                                        </div>

                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>



            </div>
        </div>



@endsection
