@extends('layouts.admin.app')

@section('content')

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-10 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">
                <form action="{{ route('product.update',$product->ID_CATEGORIA) }}" method="post">

                    @csrf
                    {{method_field('PATCH')}}

                    <div class="form-group">
                            <label for="name_user">Familia Producto:</label>
                            <select name="ID_FAMILIA" id="" class="form-control">
                                <option value="" selected disabled>Selecccione...</option>
                                <option value="null">NULL</option>
                                <option value="null">NULL</option>
                            </select>
                    </div>
                    <div class="form-group">
                        <label for="documento">CATEGORIA:</label>
                        <input name="CATEGORIA" id="CATEGORIA" type="text" class="form-control" value="{{$product->CATEGORIA}}">
                    </div>

                    <div class="form-group">
                        <label for="primer_nombre">SUBCATEGORIA:</label>
                        <input name="SUBCATEGORIA" id="SUBCATEGORIA" type="text" class="form-control" value="{{$product->SUBCATEGORIA}}">
                    </div>

                    <div class="form-group">
                        <label for="segundo_nombre">STOCK:</label>
                        <input name="STOCK" id="STOCK" type="text" class="form-control" value="{{$product->STOCK}}">
                    </div>

                    <div class="form-group">
                        <label for="apellido">DESCRIPCION:</label>
                        <input name="DESCRIPCION" id="DESCRIPCION" type="text" class="form-control" value="{{$product->DESCRIPCION}}">
                    </div>

                    <div class="form-group">
                        <label for="direccion">PRECIO:</label>
                        <input name="PRECIO" id="PRECIO_UNIDAD" type="text" class="form-control" value="{{$product->PRECIO}}">
                    </div>

                    <div class="form-group">
                        <label for="ciudad">ESTADO:</label>
                        <input name="ESTADO" id="ESTADO" type="text" class="form-control" value="{{$product->ESTADO}}">
                    </div>

                    <div class="form-group">
                        <label for="ciudad">NUMERO DE ESTRELLAS:</label>
                        <input name="NUMERO_ESTRELLAS" id="TIPO_PRODUCTO" type="text" class="form-control" value="{{$product->NUMERO_ESTRELLAS}}">
                    </div>


                    <input class="btn btn-success my-3" type="submit" value="Actualizar Producto">

                </form>

            </div>
        </div>
    </div>

@endsection
