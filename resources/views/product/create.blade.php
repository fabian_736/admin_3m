@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        <div class="row mb-5 mx-auto">
            <div class="col-md-10 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">
                <form action="{{ route('product.store') }}" method="post">
                    @csrf
                    <div class="text-center">
                        <label for="" class="lead">Registrar producto</label>
                    </div>
                    <hr>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-12">
                            <label for="name_user">Familia Producto:</label>
                            <select name="ID_FAMILIA" id="" class="form-control">
                                <option value="" selected disabled>Selecccione...</option>
                                <option value="null">NULL</option>
                                <option value="null">NULL</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-6">
                            <label for="name_user">Categoria:</label>
                            <input name="CATEGORIA" id="categoria" type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Subcategoria:</label>
                            <input name="SUBCATEGORIA" id="SUBCATEGORIA" type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Stock:</label>
                            <input name="STOCK" id="categoria" type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Descripcion:</label>
                            <input name="DESCRIPCION" id="SUBCATEGORIA" type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Precio Unidad:</label>
                            <input name="PRECIO" id="categoria" type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Estado:</label>
                            <input name="ESTADO" id="categoria" type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-12">
                            <label for="name_user">Numero de estrelllas:</label>
                            <input name="NUMERO_ESTRELLAS" id="SUBCATEGORIA" type="text" class="form-control">
                        </div>

                    </div>

                    <hr>
                    <div class="row col-md-12 my-3">
                        <button type="submit" class="btn btn-primary form-control">Guardar Producto</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


@endsection
