const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);

    mix.styles([
        'resources/design/css/sb-admin-2.css',
        'resources/design/css/sb-admin-2.min.css',
    ], 'public/css/app.css')
    
        .scripts([
            'resources/design/js/bootstrap.min.js',
            'resources/design/js/jquery-3.3.1.slim.min.js',
            'resources/design/js/popper.min.js',
        ], 'public/js/app.js');