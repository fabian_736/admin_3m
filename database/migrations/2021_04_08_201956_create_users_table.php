<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('3m_usuarios', function (Blueprint $table) {
            $table->id('ID_USUARIO');
            $table->string('USER',45);
            $table->string('CONTRASEÑA',45);
            $table->string('NOMBRES',45);
            $table->string('APELLIDOS',45);
            $table->string('CELULAR',45);
            $table->string('PRIVILEGIOS',45);
            $table->string('ESTADO',45);
            $table->string('TIPO_ODONTOLOGO_USER',45)->nullable();
            $table->string('CONTRASENA_FECHA',45)->nullable();
            $table->string('INTENTOS',45)->nullable();
            $table->string('ESTADO_LOGIN',45)->nullable();
            $table->string('CIUDAD',45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
