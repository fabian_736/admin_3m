<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('3m_categorias', function (Blueprint $table) {
            $table->id('ID_CATEGORIA');
            $table->string('CATEGORIA', 50);
            $table->string('SUBCATEGORIA', 50);
            $table->string('STOCK', 50);
            $table->string('DESCRIPCION', 500);
            $table->string('PRECIO', 25);
            $table->string('ESTADO', 20);
            $table->string('ID_FAMILIA', 20)->nullable();
            $table->string('NUMERO_ESTRELLAS', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
