<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table='3m_categorias';
    protected $primaryKey='ID_CATEGORIA';
    protected $fillable =[
        'ID_CATEGORIA',
        'CATEGORIA',
        'SUBCATEGORIA',
        'STOCK',
        'DESCRIPCION',
        'PRECIO',
        'ESTADO',
        'ID_FAMILIA',
        'NUMERO_ESTRELLAS'
    ];
}
