<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $table='3m_usuarios';
    protected $primaryKey='ID_USUARIO';
    protected $fillable =[
        'ID_USUARIO',
        'USER',
        'CONTRASEÑA',
        'NOMBRES',
        'APELLIDOS',
        'CELULAR',
        'PRIVILEGIOS',
        'TIPO_ODONTOLOGO_USER',
        'INTENTOS',
        'ESTADO_LOGIN',
        'CIUDAD'
    ];

    protected $hidden=[
        'CONTRASEÑA'
    ];
}
