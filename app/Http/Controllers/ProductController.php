<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
        return view('product.index');

    }

    public function list(){
        $products=Product::all();
        return view('product.list',compact('products'));
    }

    public function create(){
        return view('product.create');
    }

    public function store(Request $request){

        Product::create($request->all());
        return redirect('product')->with('create', 'Producto Registrado');
    }

    public function confirm($ID_CATEGORIA){
        $product = Product::findOrFail($ID_CATEGORIA);
        return view('product.confirm', compact('product'));
    }

    public function destroy($id){
        // SELECT * FROM entidad WHERE ID = ?
        // TRUE = DELETE FROM ENTIDAD WHERE ID = ?
        Product::find($id)->delete();
        return redirect('product')->with('delete', 'Producto Eliminado');
    }

    public function edit($id){
        $product = Product::findOrFail($id);
        return view('product.edit', compact('product'));
    }

    public function update(Request $request, $id){
        $datosexception = request()->except(['_token', '_method']);
        Product::where('ID_CATEGORIA', '=', $id)->update($datosexception);
        return redirect('product')->with('edit', 'Producto Actualizado');
    }

}
