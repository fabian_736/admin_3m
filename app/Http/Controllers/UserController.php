<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
class UserController extends Controller
{
    public function index()
    {
        return view('user.index');

    }

    public function listclient(){
        $users=User::all();
        return view('user.listclient',compact('users'));
    }

    public function listadviser(){
        $users=User::all();
        return view('user.listadviser',compact('users'));
    }

    //GET
    public function create(){
        return view('user.create');
    }

    public function store(Request $request){

        User::create($request->all());
        return redirect('user')->with('create', 'Usuario Registrado');
    }

    public function confirm($ID_USUARIO){
        $user = User::findOrFail($ID_USUARIO);
        return view('user.confirm', compact('user'));
    }

    public function destroy($id){
        // SELECT * FROM entidad WHERE ID = ?
        // TRUE = DELETE FROM ENTIDAD WHERE ID = ?
        User::find($id)->delete();
        return redirect('user')->with('delete', 'Usuario Eliminado');
    }

    public function edit($id){
        $user = User::findOrFail($id);
        return view('user.edit', compact('user'));
    }

    public function update(Request $request, $id){
        $datosexception = request()->except(['_token', '_method']);
        User::where('ID_USUARIO', '=', $id)->update($datosexception);
        return redirect('user')->with('edit', 'Usuario Actualizado');
    }

}
