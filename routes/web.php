<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('escritorio',[DashboardController::class,'index'])->name('dashboard.index');

Route::get('login',[LoginController::class,'form_login'])->name('login.form_login');
Route::post('auth',[LoginController::class,'auth'])->name('login.auth');
Route::get('logout',[LoginController::class,'logout'])->name('login.logout');

Route::get('user',[UserController::class,'index'])->name('user.index');
Route::get('user/list/client',[UserController::class,'listclient'])->name('user.listclient');
Route::get('user/list/adviser',[UserController::class,'listadviser'])->name('user.listadviser');
Route::get('user/create',[UserController::class,'create'])->name('user.create');
Route::post('user',[UserController::class,'store'])->name('user.store');
Route::delete('user/{id}',[UserController::class,'destroy'])->name('user.destroy');
Route::get('user/{id}/edit',[UserController::class,'edit'])->name('user.edit');
Route::patch('user/update/{id}',[UserController::class,'update'])->name('user.update');
Route::get('user/confirm/{id}',[UserController::class,'confirm'] )->name('user.confirm');

Route::get('product',[ProductController::class,'index'])->name('product.index');
Route::get('product/list',[ProductController::class,'list'])->name('product.list');
Route::get('product/create',[ProductController::class,'create'])->name('product.create');
Route::post('product',[ProductController::class,'store'])->name('product.store');
Route::delete('product/{id}',[ProductController::class,'destroy'])->name('product.destroy');
Route::get('product/{id}/edit',[ProductController::class,'edit'])->name('product.edit');
Route::patch('product/update/{id}',[ProductController::class,'update'])->name('product.update');
Route::get('product/confirm/{id}',[ProductController::class,'confirm'] )->name('product.confirm');


